﻿using Moq;
using SuperCarte.Core.Models;
using SuperCarte.Core.Services;
using SuperCarte.WPF.Aides;
using SuperCarte.WPF.ViewModels;

namespace SuperCarte.UTest.WPF.ViewModels;

/// <summary>
/// Tests unitaires pour la classe ListeCategoriesVM
/// </summary>
public class ListeCategoriesVMTest
{
    [Fact]
    public void SupprimerCommande_CategorieNonSelectionnee_NePeutSupprimer()
    {
        //Arrangement (Arrange)
        var navigateur = new Mock<INavigateur>();
        var notification = new Mock<INotification>();

        var categorieService = new Mock<ICategorieService>();        

        var authentificateur = new Mock<IAuthentificateur>();
        //L'utilisateur est autorisé
        authentificateur.Setup(x => x.EstAutorise(It.IsAny<string[]>())).Returns(true);
        authentificateur.Setup(x => x.EstAutoriseAsync(It.IsAny<string[]>())).ReturnsAsync(true);

        ListeCategoriesVM listeCategoriesVM = new ListeCategoriesVM(authentificateur.Object,
                                                                    notification.Object,
                                                                    categorieService.Object,
                                                                    navigateur.Object);

        //Action (Act)
        listeCategoriesVM.CategorieSelection = null;
        bool canExecuteActuel = listeCategoriesVM.SupprimerCommande.CanExecute(It.IsAny<object>());

        //Assertion (Assert)
        Assert.False(canExecuteActuel);
    }

    [Fact]
    public void SupprimerCommande_CategorieAvecDependance_NePeutSupprimer()
    {
        //Arrangement (Arrange)
        var navigateur = new Mock<INavigateur>();
        var notification = new Mock<INotification>();

        var categorieService = new Mock<ICategorieService>();
        categorieService.Setup(x => x.ObtenirDependance(It.IsAny<int>())).Returns(new CategorieDependance() { NbCartes = 1 });

        var authentificateur = new Mock<IAuthentificateur>();
        //L'utilisateur est autorisé
        authentificateur.Setup(x => x.EstAutorise(It.IsAny<string[]>())).Returns(true);
        authentificateur.Setup(x => x.EstAutoriseAsync(It.IsAny<string[]>())).ReturnsAsync(true);

        ListeCategoriesVM listeCategoriesVM = new ListeCategoriesVM(authentificateur.Object,
                                                                    notification.Object,
                                                                    categorieService.Object,
                                                                    navigateur.Object);

        //Action (Act)        
        listeCategoriesVM.CategorieSelection = new CategorieModel();

        bool canExecuteActuel = listeCategoriesVM.SupprimerCommande.CanExecute(It.IsAny<object>());

        //Assertion (Assert)
        Assert.False(canExecuteActuel);
    }

    [Fact]
    public void SupprimerCommande_CategorieDepandenceNull_NePeutSupprimer()
    {
        //Arrangement (Arrange)
        var navigateur = new Mock<INavigateur>();
        var notification = new Mock<INotification>();

        var categorieService = new Mock<ICategorieService>();
        categorieService.Setup(x => x.ObtenirDependance(It.IsAny<int>())).Returns((CategorieDependance)null);

        var authentificateur = new Mock<IAuthentificateur>();
        //L'utilisateur est autorisé
        authentificateur.Setup(x => x.EstAutorise(It.IsAny<string[]>())).Returns(true);
        authentificateur.Setup(x => x.EstAutoriseAsync(It.IsAny<string[]>())).ReturnsAsync(true);

        ListeCategoriesVM listeCategoriesVM = new ListeCategoriesVM(authentificateur.Object,
                                                                    notification.Object,
                                                                    categorieService.Object,
                                                                    navigateur.Object);        

        //Action (Act)        
        listeCategoriesVM.CategorieSelection = new CategorieModel();
        bool canExecuteActuel = listeCategoriesVM.SupprimerCommande.CanExecute(It.IsAny<object>());

        //Assertion (Assert)
        Assert.False(canExecuteActuel);
    }

    [Fact]
    public void SupprimerCommande_CategorieSansDependance_PeutSupprimer()
    {
        //Arrangement (Arrange)
        var navigateur = new Mock<INavigateur>();
        var notification = new Mock<INotification>();

        var categorieService = new Mock<ICategorieService>();
        categorieService.Setup(x => x.ObtenirDependance(It.IsAny<int>())).Returns(new CategorieDependance() { NbCartes = 0 });

        var authentificateur = new Mock<IAuthentificateur>();
        //L'utilisateur est autorisé
        authentificateur.Setup(x => x.EstAutorise(It.IsAny<string[]>())).Returns(true);
        authentificateur.Setup(x => x.EstAutoriseAsync(It.IsAny<string[]>())).ReturnsAsync(true);

        ListeCategoriesVM listeCategoriesVM = new ListeCategoriesVM(authentificateur.Object,
                                                                    notification.Object,
                                                                    categorieService.Object,
                                                                    navigateur.Object);

        //Action (Act)        
        listeCategoriesVM.CategorieSelection = new CategorieModel();
        bool canExecuteActuel = listeCategoriesVM.SupprimerCommande.CanExecute(It.IsAny<object>());

        //Assertion (Assert)
        Assert.True(canExecuteActuel);
    }

    [Fact]
    public void CategorieSelection_ModificationValeur_NotifierCommande()
    {
        //Arrangement (Arrange)
        var navigateur = new Mock<INavigateur>();
        var notification = new Mock<INotification>();
        var categorieService = new Mock<ICategorieService>();        

        var authentificateur = new Mock<IAuthentificateur>();
        //L'utilisateur est autorisé
        authentificateur.Setup(x => x.EstAutorise(It.IsAny<string[]>())).Returns(true);
        authentificateur.Setup(x => x.EstAutoriseAsync(It.IsAny<string[]>())).ReturnsAsync(true);

        CategorieModel? categorieModel1 = new CategorieModel();
        CategorieModel? categorieModel2 = null;
        CategorieModel? categorieModel3 = new CategorieModel();

        ListeCategoriesVM listeCategoriesVM = new ListeCategoriesVM(authentificateur.Object,
                                                                    notification.Object,
                                                                    categorieService.Object,
                                                                    navigateur.Object);

        int nbCanExecuteChangedActuel = 0;

        listeCategoriesVM.SupprimerCommande.CanExecuteChanged += delegate
        {
            nbCanExecuteChangedActuel++;
        };

        //Action (Act)        
        listeCategoriesVM.CategorieSelection = categorieModel1;
        listeCategoriesVM.CategorieSelection = categorieModel2;
        listeCategoriesVM.CategorieSelection = categorieModel3;
        listeCategoriesVM.CategorieSelection = categorieModel1;
        listeCategoriesVM.CategorieSelection = categorieModel1;//Ne doit pas faire changer

        //Assertion (Assert)
        Assert.Equal(4, nbCanExecuteChangedActuel);        
    }
}
