﻿using AutoFixture;
using FluentAssertions;
using Moq;
using SuperCarte.Core.Extensions;
using SuperCarte.Core.Models;
using SuperCarte.EF.Data;

namespace SuperCarte.UTest.Unitaires.Core.Extensions;

/// <summary>
/// Tests unitaires pour la classe UtilisateurMapExtension
/// </summary>
public class UtilisateurMapExtensionTest
{
    //[Fact]
    //public void VersUtilisateurModel_CreerObjet_ValeursIdentiques()
    //{
    //    //Arrangement (Arrange)
    //    const int utilisateurId = 9000;
    //    const string prenom = "TestPrenom";
    //    const string nom = "TestNom";
    //    const string nomUtilisateur = "TestNomUtilisateur";
    //    const string motPasseHash = "TestHash";
    //    const int roleId = 71;
    //    const string propTest = "TestPropTest";

    //    Utilisateur utilisateur = new Utilisateur()
    //    {
    //        UtilisateurId = utilisateurId,
    //        Prenom = prenom,
    //        Nom = nom,
    //        NomUtilisateur = nomUtilisateur,
    //        MotPasseHash = motPasseHash,
    //        RoleId = roleId,
    //        PropTest = propTest
    //    };

    //    //Action (Act)
    //    UtilisateurModel utilisateurModelActuel = utilisateur.VersUtilisateurModel();

    //    //Assertion (Assert)
    //    Assert.Equal(utilisateurId, utilisateurModelActuel.UtilisateurId);
    //    Assert.Equal(prenom, utilisateurModelActuel.Prenom);
    //    Assert.Equal(nom, utilisateurModelActuel.Nom);
    //    Assert.Equal(nomUtilisateur, utilisateurModelActuel.NomUtilisateur);
    //    Assert.Equal(roleId, utilisateurModelActuel.RoleId);
    //    Assert.Equal(propTest, utilisateurModelActuel.PropTest);
    //}    

    [Fact]
    public void VersUtilisateurModel_CreerObjet_ValeursIdentiques2()
    {
        //Arrangement (Arrange)
        var fixture = new Fixture();
        Utilisateur utilisateur =
            fixture.Build<Utilisateur>()
                .Without(u => u.Role)
                .Without(u => u.UtilisateurCarteListe)
                .Create();

        //Action (Act)
        UtilisateurModel utilisateurModelActuel = utilisateur.VersUtilisateurModel();

        //Assertion (Assert)
        utilisateurModelActuel.Should().BeEquivalentTo(utilisateur, 
            options => options.Excluding(x => x.MotPasseHash)
                              .Excluding(x => x.Role)
                              .Excluding(x => x.UtilisateurCarteListe));
    }
}