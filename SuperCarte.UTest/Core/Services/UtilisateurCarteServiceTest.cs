﻿using AutoFixture;
using FluentAssertions;
using Moq;
using SuperCarte.Core.Models;
using SuperCarte.Core.Repositories;
using SuperCarte.Core.Services;
using SuperCarte.Core.Validateurs;
using SuperCarte.EF.Data;

namespace SuperCarte.UTest.Core.Services;

/// <summary>
/// Tests unitaires pour la classe UtilisateurCarteService
/// </summary>
public class UtilisateurCarteServiceTest
{
    [Fact]
    public async Task ObtenirAsync_ParametreRepo_BonOrdre()
    {
        //Arrangement (Arrange)
        const int utilisateurId = 4;
        const int carteId = 5;

        var utilisateurCarteRepository = new Mock<IUtilisateurCarteRepo>();
        var utilisateurCarteValidateur = new Mock<IValidateurPropriete<UtilisateurCarteModel>>();

        UtilisateurCarteService utilisateurCarteService =
            new UtilisateurCarteService(utilisateurCarteRepository.Object,
                                        utilisateurCarteValidateur.Object);

        //Action (Act)
        await utilisateurCarteService.ObtenirAsync(utilisateurId, carteId);

        //Assertion (Assert)
        utilisateurCarteRepository.Verify(x => x.ObtenirParCleAsync(utilisateurId, carteId), Times.Once);
        utilisateurCarteRepository.Verify(x => x.ObtenirParCleAsync(carteId, utilisateurId), Times.Never);
    }

    [Fact]
    public async Task ObtenirAsync_DataVersModeleNonNull_ValeursIdentiques()
    {
        //Arrangement (Arrange)
        var fixture = new Fixture();        
        UtilisateurCarte utilisateurCarte =
            fixture.Build<UtilisateurCarte>()
                .Without(uc=> uc.Utilisateur)
                .Without(uc => uc.Carte)
                .Create();

        var utilisateurCarteRepository = new Mock<IUtilisateurCarteRepo>();
        utilisateurCarteRepository.Setup(x => x.ObtenirParCleAsync(utilisateurCarte.UtilisateurId, utilisateurCarte.CarteId))
            .ReturnsAsync(utilisateurCarte);

        var utilisateurCarteValidateur = new Mock<IValidateurPropriete<UtilisateurCarteModel>>();

        UtilisateurCarteService utilisateurCarteService =
            new UtilisateurCarteService(utilisateurCarteRepository.Object,
                                        utilisateurCarteValidateur.Object);

        //Action (Act)
        UtilisateurCarteModel utilisateurCarteModelActuel = 
            await utilisateurCarteService.ObtenirAsync(utilisateurCarte.UtilisateurId, utilisateurCarte.CarteId);

        //Assertion (Assert)
        utilisateurCarteModelActuel.Should()
            .BeEquivalentTo(utilisateurCarte, options => options.ExcludingMissingMembers());

    }

    [Fact]
    public async Task AjouterAsync_ModeleInvalide_RepoNonAjout()
    {
        //Arrangement (Arrange)        
        UtilisateurCarteModel utilisateurCarteModel = new UtilisateurCarteModel();

        var validationModel = new ValidationModel();
        validationModel.AssignerErreur("testPropriete", "testMessage");

        var utilisateurCarteValidateur = new Mock<IValidateurPropriete<UtilisateurCarteModel>>();
        utilisateurCarteValidateur.Setup(x => x.ValiderAsync(utilisateurCarteModel)).ReturnsAsync(validationModel);

        var utilisateurCarteRepository = new Mock<IUtilisateurCarteRepo>();        

        UtilisateurCarteService utilisateurCarteService =
            new UtilisateurCarteService(utilisateurCarteRepository.Object,
                                        utilisateurCarteValidateur.Object);

        //Action (Act)        
        await utilisateurCarteService.AjouterAsync(utilisateurCarteModel);

        //Assertion (Assert)
        utilisateurCarteValidateur.Verify(x => x.ValiderAsync(utilisateurCarteModel), Times.Once);
        utilisateurCarteRepository.Verify(x => x.AjouterAsync(It.IsAny<UtilisateurCarte>(), It.IsAny<bool>()), Times.Never);
    }
}
