﻿using Microsoft.Extensions.Localization;
using Moq;
using SuperCarte.Core.Models;
using SuperCarte.Core.Repositories;
using SuperCarte.Core.Resx;
using SuperCarte.Core.Validateurs;
using System.Globalization;

namespace SuperCarte.UTest.Core.Validateurs;

/// <summary>
/// Tests unitaires pour la classe UtilisateurCarteValidateur
/// </summary>
public class UtilisateurCarteValidateurTest
{
    [Theory]
    [InlineData(1)] //Min
    [InlineData(short.MaxValue)] //Max
    [InlineData(15657)] //Entre les 2
    public async Task ValiderAsync_Quantite_Valide(short quantite)
    {
        //Arrangement (Arrange)
        UtilisateurCarteModel utilisateurCarteModel = new UtilisateurCarteModel()
        {
            Quantite = quantite
        };

        var utilisateurCarteRepo = new Mock<IUtilisateurCarteRepo>();
        var carteRepo = new Mock<ICarteRepo>();
        var utilisateurRepo = new Mock<IUtilisateurRepo>();
        var ressource = new Mock<IStringLocalizer<ResUtilisateurCarteValidateur>>();
        ressource.Setup(x => x[It.IsAny<string>()]).Returns(new LocalizedString("Test", "Message erreur Test"));

        UtilisateurCarteValidateur utilisateurCarteValidateur =
            new UtilisateurCarteValidateur(utilisateurCarteRepo.Object,
                                           utilisateurRepo.Object,
                                           carteRepo.Object,
                                           ressource.Object);

        //Action (Act)
        ValidationModel validationModel = await utilisateurCarteValidateur.ValiderAsync(utilisateurCarteModel);

        //Assertion (Assert)        
        Assert.False(validationModel.ErreurParPropriete.ContainsKey(nameof(utilisateurCarteModel.Quantite)));
    }

    [Theory]
    [InlineData(0)] //Limite
    [InlineData(-10)] //Négatif    
    public async Task ValiderAsync_Quantite_NonValide(short quantite)
    {
        //Arrangement (Arrange)        
        const string messageErreurAttendu = "Quantite_PlageInvalide_Test";

        UtilisateurCarteModel utilisateurCarteModel = new UtilisateurCarteModel()
        {
            Quantite = quantite
        };

        var utilisateurCarteRepo = new Mock<IUtilisateurCarteRepo>();
        var carteRepo = new Mock<ICarteRepo>();
        var utilisateurRepo = new Mock<IUtilisateurRepo>();
        var ressource = new Mock<IStringLocalizer<ResUtilisateurCarteValidateur>>();
        ressource.Setup(x => x["Quantite_PlageInvalide"]).Returns(new LocalizedString("Quantite_PlageInvalide", messageErreurAttendu));       

        UtilisateurCarteValidateur utilisateurCarteValidateur =
            new UtilisateurCarteValidateur(utilisateurCarteRepo.Object,
                                           utilisateurRepo.Object,
                                           carteRepo.Object,
                                           ressource.Object);

        //Action (Act)
        ValidationModel validationModel = await utilisateurCarteValidateur.ValiderAsync(utilisateurCarteModel);

        //Assertion (Assert)        
        Assert.True(validationModel.ErreurParPropriete.ContainsKey(nameof(utilisateurCarteModel.Quantite)));
        Assert.Equal(messageErreurAttendu, validationModel.ErreurParPropriete[nameof(utilisateurCarteModel.Quantite)]);
    }
}
