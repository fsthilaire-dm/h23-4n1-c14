﻿namespace SuperCarte.EF.Data;

public class Utilisateur
{
    public int UtilisateurId { get; set; }

    public string Prenom { get; set; } = null!;

    public string Nom { get; set; } = null!;

    public string NomUtilisateur { get; set; } = null!;

    public string MotPasseHash { get; set; } = null!;    

    public int RoleId { get; set; }

    public Role Role { get; set; } = null!;

    public ICollection<UtilisateurCarte> UtilisateurCarteListe { get; set; } = new List<UtilisateurCarte>();
}