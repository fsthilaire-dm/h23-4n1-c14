﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace SuperCarte.EF.Migrations
{
    /// <inheritdoc />
    public partial class CreationBD : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "CategorieTb",
                columns: table => new
                {
                    CategorieId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nom = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CategorieTb", x => x.CategorieId);
                });

            migrationBuilder.CreateTable(
                name: "EnsembleTb",
                columns: table => new
                {
                    EnsembleId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nom = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Disponibilite = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EnsembleTb", x => x.EnsembleId);
                });

            migrationBuilder.CreateTable(
                name: "RoleTb",
                columns: table => new
                {
                    RoleId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nom = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RoleTb", x => x.RoleId);
                });

            migrationBuilder.CreateTable(
                name: "CarteTb",
                columns: table => new
                {
                    CarteId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nom = table.Column<string>(type: "nvarchar(max)", nullable: false),                    
                    Vie = table.Column<short>(type: "smallint", nullable: false),
                    Armure = table.Column<short>(type: "smallint", nullable: false),
                    Attaque = table.Column<short>(type: "smallint", nullable: false),
                    EstRare = table.Column<bool>(type: "bit", nullable: false),
                    PrixRevente = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    CategorieId = table.Column<int>(type: "int", nullable: false),
                    EnsembleId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CarteTb", x => x.CarteId);
                    table.ForeignKey(
                        name: "FK_CarteTb_CategorieTb_CategorieId",
                        column: x => x.CategorieId,
                        principalTable: "CategorieTb",
                        principalColumn: "CategorieId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CarteTb_EnsembleTb_EnsembleId",
                        column: x => x.EnsembleId,
                        principalTable: "EnsembleTb",
                        principalColumn: "EnsembleId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UtilisateurTb",
                columns: table => new
                {
                    UtilisateurId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Prenom = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Nom = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    NomUtilisateur = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    MotPasseHash = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    RoleId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UtilisateurTb", x => x.UtilisateurId);
                    table.ForeignKey(
                        name: "FK_UtilisateurTb_RoleTb_RoleId",
                        column: x => x.RoleId,
                        principalTable: "RoleTb",
                        principalColumn: "RoleId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CarteTb_CategorieId",
                table: "CarteTb",
                column: "CategorieId");

            migrationBuilder.CreateIndex(
                name: "IX_CarteTb_EnsembleId",
                table: "CarteTb",
                column: "EnsembleId");

            migrationBuilder.CreateIndex(
                name: "IX_UtilisateurTb_RoleId",
                table: "UtilisateurTb",
                column: "RoleId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CarteTb");

            migrationBuilder.DropTable(
                name: "UtilisateurTb");

            migrationBuilder.DropTable(
                name: "CategorieTb");

            migrationBuilder.DropTable(
                name: "EnsembleTb");

            migrationBuilder.DropTable(
                name: "RoleTb");
        }
    }
}
