﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using SuperCarte.EF.Data.Context;
using SuperCarte.ITest.Aides;
using SuperCarte.WPF.Aides;
using SuperCarte.WPF.Extensions.ServiceCollections;

namespace SuperCarte.ITest;

public class Startup
{
    /// <summary>
    /// Méhthode qui permet d'enregistrer les services
    /// </summary>
    /// <param name="services">Collection de services</param>
    /// <param name="context">Context de l'application</param>
    public void ConfigureServices(IServiceCollection services,
        HostBuilderContext context)
    {
        services.AddLocalization();
        services.AddDbContext<SuperCarteContext>(options => options.UseSqlServer(context.Configuration.GetConnectionString("DefaultConnection")));

        services.AddScoped<UtilitaireBD>();
                
        services.AddSingleton<IAuthentificateur, Authentificateur>();        

        services.EnregistrerRepositories();
        services.EnregistrerServices();
        services.EnregistrerValidateurs();
        services.EnregistrerViewModels();
    }

    public void ConfigureHost(IHostBuilder hostBuilder) =>
        hostBuilder.ConfigureHostConfiguration(builder => {
            builder.AddJsonFile("appsettings.json");
        });

}
