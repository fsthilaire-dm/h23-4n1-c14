﻿using AutoFixture;
using Microsoft.EntityFrameworkCore;
using SuperCarte.EF.Data;
using SuperCarte.EF.Data.Context;

namespace SuperCarte.ITest.Aides;

/// <summary>
/// Classe qui permet de gérer la base de données pour les tests
/// </summary>
public class UtilitaireBD
{
    private readonly SuperCarteContext _bd;

    /// <summary>
    /// Constructeur
    /// </summary>
    /// <param name="bd">Contexte de la base de données</param>
    public UtilitaireBD(SuperCarteContext bd)
    {
        _bd = bd;
    }

    /// <summary>
    /// Initialise la base de données Test et applique la migration
    /// </summary>
    public void Initialiser()
    {
        //Supprime la base de données si elle existe
        _bd.Database.EnsureDeleted();

        //Applique la dernière migration 
        //Équivalent de Update-Database
        _bd.Database.Migrate();        
    }

    public SuperCarteContext BDContext 
    { 
        get 
        { 
            return _bd; 
        } 
    }

    public Categorie CreerObjetCategorieValide()
    {
        var fixture = new Fixture();

        //Création de l'objet avec Autofixture pour s'assurer que toutes les propriétés ont une valeur
        Categorie categorie =
            fixture.Build<Categorie>()
                .Without(u => u.CategorieId)
                .Without(u => u.CarteListe)
                .Create();

        //Assignation des valeurs valides
        categorie.Nom = "Nom valide";
        categorie.Description = "Description valide";

        return categorie;
    }

    /// <summary>
    /// Obtenir le nom de l'utilisateur et le mot de passe d'un utilisateur le rôle Administrateur
    /// </summary>
    /// <returns>Tuple (nomUtilisater, motPasse)</returns>
    public (string nomUtilisateur, string motPasse) ObtenirUtilisateurRoleAdministrateur()
    {
        return ("fsthilaire", "Native3!");
    }

    /// <summary>
    /// Obtenir le nom de l'utilisateur et le mot de passe d'un utilisateur avec le rôle Utilisateur
    /// </summary>
    /// <returns>Tuple (nomUtilisater, motPasse)</returns>
    public (string nomUtilisateur, string motPasse) ObtenirUtilisateurRoleUtilisateur()
    {
        return ("tstark", "#NotAdmin!");
    }

    //public Categorie CreerObjetCategorieValide()
    //{
    //    Categorie categorie = new Categorie();

    //    //Assignation des valeurs valides
    //    categorie.Nom = "Nom valide";
    //    //Oubli volontaire de Description

    //    return categorie;
    //}
}
