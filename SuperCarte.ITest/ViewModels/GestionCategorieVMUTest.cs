﻿using FluentAssertions;
using SuperCarte.Core.Services;
using SuperCarte.EF.Data;
using SuperCarte.ITest.Aides;
using SuperCarte.WPF.ViewModels;

namespace SuperCarte.ITest.ViewModels;

/// <summary>
/// Test d'intégration à partir de GestionCategorieVM 
/// </summary>
public class GestionCategorieVMUTest
{    
    private readonly UtilitaireBD _utilitaireBD;    
    private readonly ICategorieService _categorieService;

    /// <summary>
    /// Constructeur
    /// </summary>
    /// <param name="utilitaireBD">Utilitaire de la BD</param>
    /// <param name="categorieService">Service du module Categorie</param>
    public GestionCategorieVMUTest(UtilitaireBD utilitaireBD,
        ICategorieService categorieService)
	{        
        _utilitaireBD = utilitaireBD;        
        _categorieService = categorieService;
    }

    [Fact]
    public async Task ObtenirCommande_CategorieExiste_ProprieteOK()
    {
        //Arrangement (Arrange)
        const int categorieId = 1;

        ////Initialisation de la base de données test
        _utilitaireBD.Initialiser();

        ////Obtenir la catégorie attendue
        Categorie? categorieAttendue = _utilitaireBD.BDContext.CategorieTb.Where(c => c.CategorieId == categorieId).FirstOrDefault();

        ////Création du ViewModel
        GestionCategorieVM gestionCategorieVM = new GestionCategorieVM(_categorieService);

        //Action (Act)         
        ////Assignation de la catégorie à gérer
        gestionCategorieVM.AssignerParametre(categorieId);

        ////Exécution de la commande pour obtenir
        await gestionCategorieVM.ObtenirCommande.ExecuteAsync(null);
                
        //Assertion (Assert)        
        ////La catégorie à afficher existe dans la base de données
        Assert.NotEqual((Categorie?)null, categorieAttendue); 

        ////Les propriétés de liaison sont identiques à celle de la base de donnée
        Assert.Equal(categorieAttendue.CategorieId, gestionCategorieVM.CategorieId);
        Assert.Equal(categorieAttendue.Nom, gestionCategorieVM.Nom);
        Assert.Equal(categorieAttendue.Description, gestionCategorieVM.Description);
    }

    [Fact]
    public async Task EnregistrerCommande_AjouterCategorieValide_ExisteBD()
    {
        //Arrangement (Arrange)
        ////Initialisation de la base de données test
        _utilitaireBD.Initialiser();

        ////Objet categorie a ajouté dans la base de données
        Categorie categorieAttendue = _utilitaireBD.CreerObjetCategorieValide();        

        ////Obtenir la liste des CategorieId existant pour s'assurer que la catégorie n'existe pas déjà
        List<int> lstCategorieIdInitial = _utilitaireBD.BDContext.CategorieTb.Select(c => c.CategorieId).ToList();        

        ////Création du ViewModel
        GestionCategorieVM gestionCategorieVM = new GestionCategorieVM(_categorieService);

        //Action (Act)         
        ////Assignation de la catégorie à créer
        gestionCategorieVM.AssignerParametre(0);
        
        ////Assignation des propriétés de liaison à partir de l'objet à créer
        gestionCategorieVM.Nom = categorieAttendue.Nom;
        gestionCategorieVM.Description = categorieAttendue.Description;

        ////Exécution de la commande enregistrer
        await gestionCategorieVM.EnregistrerCommande.ExecuteAsync(null);

        ////Obtenir la catégorie dans la base de données à partir de la clé générée
        Categorie? categorieAjoutee = _utilitaireBD.BDContext.CategorieTb.Where(c => c.CategorieId == gestionCategorieVM.CategorieId).FirstOrDefault();

        //Assertion (Assert)
        ////Vérifie que la clé générée n'est pas 0
        Assert.NotEqual(gestionCategorieVM.CategorieId, 0);

        ////Vérifie que la catégorie ajoutée existe dans la base de données
        Assert.NotEqual((Categorie?)null, categorieAjoutee);        

        ////Vérifie que la clé de la catégorie ajoutée n'existait pas avant l'ajout
        Assert.False(lstCategorieIdInitial.Contains(gestionCategorieVM.CategorieId));

        ////Vérifie que les propriétés sont identiques dans la base de données et l'objet créé
        categorieAjoutee.Should().BeEquivalentTo(categorieAttendue, 
            options => options.Excluding(x => x.CategorieId)
                              .Excluding(x => x.CarteListe));
    }
}
