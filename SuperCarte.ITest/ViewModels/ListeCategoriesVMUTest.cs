﻿using Moq;
using SuperCarte.Core.Services;
using SuperCarte.EF.Data;
using SuperCarte.ITest.Aides;
using SuperCarte.WPF.Aides;
using SuperCarte.WPF.ViewModels;

namespace SuperCarte.ITest.ViewModels;

/// <summary>
/// Test d'intégration à partir de ListeCategoriesVM 
/// </summary>
public class ListeCategoriesVMUTest
{
    private readonly UtilitaireBD _utilitaireBD;
    private readonly ICategorieService _categorieService;
    private readonly IAuthentificateur _authentificateur;

    /// <summary>
    /// Constructeur
    /// </summary>
    /// <param name="utilitaireBD">Utilitaire de la BD</param>
    /// <param name="categorieService">Service du module Categorie</param>
    /// <param name="authentificateur">Classe d'assistance pour l'authentification</param>
    public ListeCategoriesVMUTest(UtilitaireBD utilitaireBD,
        ICategorieService categorieService,
        IAuthentificateur authentificateur)
    {
        _utilitaireBD = utilitaireBD;
        _categorieService = categorieService;
        _authentificateur = authentificateur;
    }

    [Fact]
    public async Task CreerVM_UtilisateurNonAutorise_CommandesNull()
    {
        //Arrangement (Arrange)
        ////Initialisation de la base de données test
        _utilitaireBD.Initialiser();

        ////Connecter un utilisateur qui a le rôle Administrateur
        var compteUtilisateur = _utilitaireBD.ObtenirUtilisateurRoleUtilisateur();
        await _authentificateur.AuthentifierUtilisateurAsync(compteUtilisateur.nomUtilisateur, compteUtilisateur.motPasse);

        ////Création des simulacres pour les services dépendants du UI
        var navigateur = new Mock<INavigateur>();
        var notification = new Mock<INotification>();


        //Action (Act)
        ////Création du ViewModel
        ListeCategoriesVM listeCategorieVM = new ListeCategoriesVM(_authentificateur,
                                                                   notification.Object,
                                                                   _categorieService,
                                                                   navigateur.Object);

        //Assertion (Assert)
        ////Vérifie que le message d'erreur a été affiché
        notification.Verify(x => x.MessageErreur(It.IsAny<string>(), It.IsAny<string>()), Times.Once);
        
        ////Vérifie que les commandes sont tous null
        Assert.True(listeCategorieVM.ObtenirListeCommande is null);
        Assert.True(listeCategorieVM.NouveauCommande is null);
        Assert.True(listeCategorieVM.EditerCommande is null);
        Assert.True(listeCategorieVM.SupprimerCommande is null);
    }

    [Fact]
    public async Task SupprimerCommande_CategorieSansDependance_OK()
    {
        ////Arrangement (Arrange)
        ////Catégorie sans dépendance
        const int categorieId = 3;

        ////Initialisation de la base de données test
        _utilitaireBD.Initialiser(); 

        ////Obtenir la catégorie à supprimer
        Categorie categorieASupprimer = _utilitaireBD.BDContext.Find<Categorie>(categorieId); 

        ////Connecter un utilisateur qui a le rôle Administrateur
        var compteUtilisateur = _utilitaireBD.ObtenirUtilisateurRoleAdministrateur();
        await _authentificateur.AuthentifierUtilisateurAsync(compteUtilisateur.nomUtilisateur, compteUtilisateur.motPasse);

        ////Création des simulacres pour les services dépendants du UI
        var navigateur = new Mock<INavigateur>();
        var notification = new Mock<INotification>();
        
        ////Création du ViewModel
        ListeCategoriesVM listeCategorieVM = new ListeCategoriesVM(_authentificateur,
                                                                   notification.Object,
                                                                   _categorieService,
                                                                   navigateur.Object);

        //Action (Act)
        ////Exécution de la commande pour obtenir la liste
        await listeCategorieVM.ObtenirListeCommande.ExecuteAsync(null);
        
        ////Obtenir la liste des catégories id avant la suppression pour l'assertion
        List<int> lstCategorieIdAvantSupp = listeCategorieVM.ListeCategories.Select(c => c.CategorieId).ToList();

        ////Sélection de la catégorie à supprimer
        listeCategorieVM.CategorieSelection = listeCategorieVM.ListeCategories.Where(c => c.CategorieId == categorieId).FirstOrDefault();

        ////Exécuter la commande de suppression
        await listeCategorieVM.SupprimerCommande.ExecuteAsync(null);

        ////Obtenir la liste des CategorieId après la suppression pour l'assertion
        List<int> lstCategorieIdApresSupp = listeCategorieVM.ListeCategories.Select(c => c.CategorieId).ToList();

        ////Obtenir dans la base de données la catégorie supprimée pour l'assertion
        Categorie? categorieSupprime = _utilitaireBD.BDContext.CategorieTb.Where(c => c.CategorieId == categorieId).FirstOrDefault();

        //Assertion (Assert)
        ////La catégorie à supprimer existe dans la base de données avant la suppression
        Assert.False(categorieASupprimer is null);

        ////La catégorie à supprimer est dans la liste avant la suppression
        Assert.True(lstCategorieIdAvantSupp.Contains(categorieId));

        ////La liste avant la suppression a un élément de plus que la liste après la supression
        Assert.True(lstCategorieIdAvantSupp.Count - 1 == lstCategorieIdApresSupp.Count);

        ////La catégorie à supprimer n'est pas dans la liste avant la suppression
        Assert.False(lstCategorieIdApresSupp.Contains(categorieId));

        ////La catégorie à supprimer n'existe pas dans la BD après la suppression
        Assert.True(categorieSupprime is null);
    }
}