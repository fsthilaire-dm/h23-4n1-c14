﻿using System.Net.Http.Headers;
using System.Net.Http.Json;

namespace AirTableApiClient;

/// <summary>
/// Client qui permet de communiquer avec l'API de AirTable par un jeton personnel
/// </summary>
public class AirTableClient : IDisposable, IAirTableClient
{
    private readonly string _airTableUrl = "https://api.airtable.com/v0";
    private readonly HttpClient _httpClient;


    public AirTableClient()
    {
        _httpClient = new HttpClient();
    }

    /// <summary>
    /// Créer une nouvelle table dans
    /// </summary>
    /// <param name="baseId">L'identifiant de la base de AirTable</param>
    /// <param name="nomTable">Le nom de la nouvelle table</param>
    /// <param name="colonnes">La liste des colonnes de la table. Mimium 1 colonne</param>
    /// <returns>L'idenitifiant de la table</returns>
    public async Task<string> CreerTableAsync(string apiJeton, string baseId, string nomTable, params ColonneDefinition[] colonnes)
    {
        if (colonnes?.Length > 0)
        {
            AssignerJeton(apiJeton);

            TableDefinition table = new TableDefinition()
            {
                Nom = nomTable,
                Colonnes = colonnes.ToList()
            };            

            HttpResponseMessage reponse =
                await _httpClient.PostAsJsonAsync($"{_airTableUrl}/meta/bases/{baseId}/tables", table);

            if (reponse.IsSuccessStatusCode)
            {
                TableDefinition tableReponse = await reponse.Content.ReadFromJsonAsync<TableDefinition>();

                return tableReponse.Id;
            }
            else
            {
                string message = await reponse.Content.ReadAsStringAsync();
                throw new Exception("Erreur avec la communication de l'API." + message);
            }
        }
        else
        {
            throw new Exception("Il doit avoir au minimum 1 colonne.");
        }
    }

    public async Task<string> CreerEnregistrementAsync(string apiJeton, string baseId, string tableId, Enregistrement enregistrement)
    {
        if (enregistrement?.Champs.Keys.Count > 0)
        {
            AssignerJeton(apiJeton);

            HttpResponseMessage reponse =
                    await _httpClient.PostAsJsonAsync($"{_airTableUrl}/{baseId}/{tableId}", enregistrement);
                        
            if (reponse.IsSuccessStatusCode)
            {
                Enregistrement enregistrementReponse = await reponse.Content.ReadFromJsonAsync<Enregistrement>();

                return enregistrementReponse.Id;
            }
            else
            {
                string message = await reponse.Content.ReadAsStringAsync();
                throw new Exception("Erreur avec la communication de l'API." + message);
            }
        }
        else
        {
            throw new Exception("Il faut au moins 1 champ dans l'enregistrement.");
        }
    }

    public void Dispose()
    {
        _httpClient.Dispose();
    }

    private void AssignerJeton(string apiJeton)
    {
        if (string.IsNullOrEmpty(apiJeton))
        {
            throw new Exception("Il faut spécifier le Jeton de l'API par la propriété ApiJeton.");
        }

        _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", apiJeton);
    }
}
