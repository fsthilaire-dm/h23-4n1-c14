﻿using System.Text.Json.Serialization;

namespace AirTableApiClient;

/// <summary>
/// Classe qui contient la définition d'une table
/// </summary>
public class TableDefinition
{
    [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
    [JsonPropertyName("id")]
    public string Id { get; set; }

    [JsonPropertyName("name")]
    public string Nom { get; set; }

    [JsonPropertyName("fields")]
    public List<ColonneDefinition> Colonnes { get; set; }
}
