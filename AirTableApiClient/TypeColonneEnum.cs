﻿namespace AirTableApiClient;

/// <summary>
/// Enumeration des types de colonne d'une table
/// </summary>
public enum TypeColonneEnum
{
    date,
    number,
    checkbox,
    singleLineText
}
