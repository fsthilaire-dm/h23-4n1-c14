﻿using System.Text.Json.Serialization;

namespace AirTableApiClient;

/// <summary>
/// Classe qui contient l'information d'un enregistrement d'une table
/// </summary>
public class Enregistrement
{
    [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
    [JsonPropertyName("id")]
    public string Id { get; set; }

    [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
    [JsonPropertyName("createdTime")]
    public DateTime? DateCreation { get; set; }

    [JsonPropertyName("fields")]
    public Dictionary<string, object> Champs { get; set; } = new Dictionary<string, object>();

    /// <summary>
    /// Ajouter un champ avec sa valeur
    /// </summary>
    /// <param name="nom">Nom du champ de la table</param>
    /// <param name="valeur">Valeur du champ</param>
    public void AjouterChamp(string nom, object valeur)
    {
        Champs.Add(nom, valeur);
    }

    /// <summary>
    /// Ajouter un champ de type date avec sa valeur
    /// </summary>
    /// <param name="nom">Nom du champ de la table</param>
    /// <param name="valeur">Valeur du champ</param>
    public void AjouterChamp(string nom, DateTime valeur)
    {
        Champs.Add(nom, valeur.ToString("yyyy-MM-dd"));
    }
}
