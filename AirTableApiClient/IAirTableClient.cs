﻿namespace AirTableApiClient;

/// <summary>
/// Interface qui représente le client qui permet de communiquer avec l'API de AirTable par un jeton personnel
/// </summary>
public interface IAirTableClient
{
    /// <summary>
    /// Créer une table dans une base en asynchrone
    /// </summary>
    /// <param name="apiJeton">Le jeton</param>
    /// <param name="baseId">L'identifiant de la base</param>
    /// <param name="nomTable">Le nom de la table</param>
    /// <param name="colonnes">La liste des colonnes de la table</param>
    /// <returns>L'identifiant de la table</returns>
    Task<string> CreerTableAsync(string apiJeton, string baseId, string nomTable, params ColonneDefinition[] colonnes);

    /// <summary>
    /// Créer un enregistremetn dans une table
    /// </summary>
    /// <param name="apiJeton">Le jeton</param>
    /// <param name="baseId">L'identifiant de la base</param>
    /// <param name="tableId">L'identifiant de la table</param>
    /// <param name="enregistrement">L'enregistrement à ajouter</param>
    /// <returns>L'identifiant de l'enregistrement</returns>
    Task<string> CreerEnregistrementAsync(string apiJeton, string baseId, string tableId, Enregistrement enregistrement);
    
    
}