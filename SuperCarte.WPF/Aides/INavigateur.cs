﻿namespace SuperCarte.WPF.Aides;

/// <summary>
/// Interface qui contient la mécanique de navigation
/// </summary>
public interface INavigateur
{
    BaseVM VMActif { get; }

    /// <summary>
    /// Naviger vers un ViewModel
    /// </summary>
    /// <typeparam name="TViewModel">Type du ViewModel</typeparam>
    void Naviguer<TViewModel>() where TViewModel : BaseVM;

    /// <summary>
    /// Naviger vers un ViewModel et assigner un paramètre initial
    /// </summary>
    /// <typeparam name="TViewModel">Type du ViewModel</typeparam>
    /// <typeparam name="TParameter">Type du paramètre</typeparam>
    /// <param name="parametre">Paramètre initial</param>
    void Naviguer<TViewModel, TParameter>(TParameter parametre) where TViewModel : BaseParametreVM<TParameter>;

    /// <summary>
    /// Quitter le programme
    /// </summary>
    void Quitter();
}
