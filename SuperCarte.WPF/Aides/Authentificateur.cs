﻿using CommunityToolkit.Mvvm.ComponentModel;
using System.Linq;

namespace SuperCarte.WPF.Aides;

/// <summary>
/// Classe qui représente la classe d'assistance pour l'authentification et l'autorisation
/// </summary>
public class Authentificateur : ObservableObject, IAuthentificateur
{
    private readonly IUtilisateurService _utilisateurService;
    private bool _estAuthentifie = false;

    /// <summary>
    /// Constructeur
    /// </summary>
    /// <param name="utilisateurService">Service du modèle utilisateur</param>
    public Authentificateur(IUtilisateurService utilisateurService)
    {
        _utilisateurService = utilisateurService;
    }

    public async Task<bool> AuthentifierUtilisateurAsync(string nomUtilisateur, string motPasse)
    {
        UtilisateurAuthentifie = await _utilisateurService.AuthentifierUtilisateurAsync(nomUtilisateur, motPasse);

        EstAuthentifie = UtilisateurAuthentifie != null;

        return EstAuthentifie;
    }

    public async Task<bool> EstAutoriseAsync(params string[] nomRoles)
    {
        if (UtilisateurAuthentifie != null)
        {
            return await _utilisateurService.AutoriserUtilisateurParRolesAsync(UtilisateurAuthentifie.UtilisateurId, nomRoles.ToList());
        }
        else
        {
            return await Task.FromResult(false);
        }
    }

    public bool EstAutorise(params string[] nomRoles)
    {
        if (UtilisateurAuthentifie != null)
        {
            return _utilisateurService.AutoriserUtilisateurParRoles(UtilisateurAuthentifie.UtilisateurId, nomRoles.ToList());
        }
        else
        {
            return false;
        }
    }

    public UtilisateurAuthentifieModel? UtilisateurAuthentifie { get; private set; }

    public bool EstAuthentifie 
    { 
        get
        {
            return _estAuthentifie;
        }
        private set
        {
            SetProperty(ref _estAuthentifie, value);
        }
    }
}
