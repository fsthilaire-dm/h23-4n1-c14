﻿using CommunityToolkit.Mvvm.Input;

namespace SuperCarte.WPF.ViewModels;

/// <summary>
/// ViewModel de la vue ListeMesCartes
/// </summary>
public class ListeMesCartesVM : BaseVM
{
    private readonly string[] _rolesAutorises = { "Administrateur", "Utilisateur" };

    #region Dépendances
    private readonly IAuthentificateur _authentificateur;
    private readonly INotification _notification;
    private readonly INavigateur _navigateur;
    private readonly IUtilisateurCarteService _utilisateurCarteService;
    #endregion

    #region Attributs des propriétés
    private List<QuantiteCarteDetailModel> _lstCartes;
    private QuantiteCarteDetailModel? _carteSelection;
    private bool _estEnTravail = false;
    #endregion

    /// <summary>
    /// Constructeur
    /// </summary>
    /// <param name="authentificateur">La classe d'assistance d'authentification</param>
    /// <param name="notification">La classe d'assistance pour la notification</param>
    /// <param name="utilisateurCarteService">Service du modèle UtilisateurCarte</param>
    /// <param name="navigateur">La classe d'assistance Navigateur</param>
    public ListeMesCartesVM(IAuthentificateur authentificateur, INotification notification,
        IUtilisateurCarteService utilisateurCarteService, INavigateur navigateur)
    {
        _authentificateur = authentificateur;
        _notification = notification;        
        if (_authentificateur.EstAutorise(_rolesAutorises))
        {
            _navigateur = navigateur;
            _utilisateurCarteService = utilisateurCarteService;

            ObtenirListeCommande = new AsyncRelayCommand(ObtenirListeAsync);
            NouveauCommande = new RelayCommand(() => _navigateur.Naviguer<GestionMesCartesVM, int>(0));
            EditerCommande = new RelayCommand(() => _navigateur.Naviguer<GestionMesCartesVM, int>(CarteSelection.CarteId),
                                              () => CarteSelection != null);
        }
        else
        {
            _notification.MessageErreur("Accès non autorisé.", "Vous n'avez pas accès à cette vue.");
        }
    }

    #region Méthodes des commandes
    /// <summary>
    /// Obtenir la liste de catégories du service
    /// </summary>    
    private async Task ObtenirListeAsync()
    {
        EstEnTravail = true;

        if (_authentificateur.EstAutorise(_rolesAutorises))
        {
            ListeCartes = await _utilisateurCarteService
                .ObtenirCartesUtilisateurAsync(_authentificateur.UtilisateurAuthentifie!.UtilisateurId);
        }
        else
        {
            _notification.MessageErreur("Accès non autorisé.", "Vous n'avez pas accès à cette fonctionnalité.");
        }

        EstEnTravail = false;
    }
    #endregion

    #region Commandes
    public IAsyncRelayCommand ObtenirListeCommande { get; private set; }

    public IRelayCommand NouveauCommande { get; private set; }

    public IRelayCommand EditerCommande { get; private set; }
    #endregion

    #region Propriétés liées
    public bool EstEnTravail
    {
        get
        {
            return _estEnTravail;
        }
        set
        {
            SetProperty(ref _estEnTravail, value);
        }
    }

    public List<QuantiteCarteDetailModel> ListeCartes
    {
        get
        {
            return _lstCartes;
        }
        set
        {
            SetProperty(ref _lstCartes, value);
        }
    }

    public QuantiteCarteDetailModel? CarteSelection
    {
        get
        {
            return _carteSelection;
        }
        set
        {
            if(SetProperty(ref _carteSelection, value))
            {
                EditerCommande.NotifyCanExecuteChanged();
            }
        }
    }
    #endregion
}
