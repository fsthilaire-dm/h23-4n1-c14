﻿using CommunityToolkit.Mvvm.Input;

namespace SuperCarte.WPF.ViewModels;

public class MainWindowVM : BaseVM
{
    private readonly INavigateur _navigateur;
    private readonly IAuthentificateur _authentificateur;

    public MainWindowVM(INavigateur navigateur, IAuthentificateur authentificateur)
	{           
        _navigateur = navigateur;
        _authentificateur = authentificateur;

        //Création des commandes
        NaviguerListeCartesVMCommande = new RelayCommand(_navigateur.Naviguer<ListeCartesVM>);
        NaviguerListeCategoriesVMCommande = new RelayCommand(_navigateur.Naviguer<ListeCategoriesVM>);
        NaviguerListeMesCartesVMCommande = new RelayCommand(_navigateur.Naviguer<ListeMesCartesVM>);

        NaviguerQuitterCommande = new RelayCommand(_navigateur.Quitter);

        //Vue initiale
        _navigateur.Naviguer<ConnexionVM>();
    }

    public IRelayCommand NaviguerListeCartesVMCommande {  get; private set; }

    public IRelayCommand NaviguerListeCategoriesVMCommande { get; private set; }

    public IRelayCommand NaviguerListeMesCartesVMCommande { get; private set; }

    public IRelayCommand NaviguerQuitterCommande { get; private set; }

    public INavigateur Navigateur
    { 
        get
        {
            return _navigateur;
        }
    }
    
    public IAuthentificateur Authentificateur
    {
        get 
        {
            return _authentificateur;
        }
    }
}
