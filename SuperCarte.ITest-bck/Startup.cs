﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using SuperCarte.EF.Data.Context;
using SuperCarte.ITest;
using SuperCarte.WPF.Extensions.ServiceCollections;

namespace SuperCarte.Test;

public class Startup
{
    /// <summary>
    /// Méhthode qui permet d'enregistrer les services
    /// </summary>
    /// <param name="services">Collection de services</param>
    /// <param name="context">Context de l'application</param>
    public void ConfigureServices(IServiceCollection services, HostBuilderContext context)
    {
        services.AddDbContext<SuperCarteContext>(options => options.UseSqlServer(context.Configuration.GetConnectionString("DefaultConnection")));
        //services.AddSingleton<InitialisationBD>();

        services.EnregistrerRepositories();
        services.EnregistrerServices();
        services.EnregistrerValidateurs();
        services.EnregistrerViewModels();
    }

    //public void ConfigureHost(IHostBuilder hostBuilder) =>
    //    hostBuilder.ConfigureHostConfiguration(builder => {
    //        builder.AddJsonFile("appsettings.json");
    //    });



    public Startup(IHostingEnvironment env)
    {
        var builder = new ConfigurationBuilder()
            .SetBasePath(env.ContentRootPath)
            .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)            
            .AddEnvironmentVariables();

        Configuration = builder.Build();
    }

    public IConfigurationRoot Configuration { get; }

   
}