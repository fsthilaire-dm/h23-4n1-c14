﻿using Microsoft.EntityFrameworkCore;
using SuperCarte.EF.Data.Context;

namespace SuperCarte.ITest;

public class InitialisationBD
{
    private readonly SuperCarteContext _bd;

    public InitialisationBD(SuperCarteContext bd)
	{
        _bd = bd;
    }

    public void Initialiser()
    {
        _bd.Database.EnsureDeleted();

        _bd.Database.Migrate();
    }
}
