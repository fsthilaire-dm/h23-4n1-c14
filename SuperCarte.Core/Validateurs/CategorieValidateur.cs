﻿using FluentValidation;
using FluentValidation.Results;
using Microsoft.Extensions.Localization;
using SuperCarte.Core.Extensions;
using SuperCarte.Core.Models;
using SuperCarte.Core.Resx;
using System.Text.RegularExpressions;

namespace SuperCarte.Core.Validateurs;

/// <summary>
/// Classe qui valide le modèle CategorieModel
/// </summary>
public class CategorieValidateur : AbstractValidator<CategorieModel>, IValidateur<CategorieModel>
{    
    private readonly IStringLocalizer<ResCategorieValidateur> _resCategorieValidateur;

    /// <summary>
    /// Constructeur
    /// </summary>    
    /// <param name="resCategorieValidateur">Fichier ressource ResCategorieValidateur</param>
    public CategorieValidateur(IStringLocalizer<ResCategorieValidateur> resCategorieValidateur)
    {        
        _resCategorieValidateur = resCategorieValidateur;

        RuleFor(i => i.Nom).Cascade(CascadeMode.Stop)
            .Must(ValiderStringObligatoire).WithMessage(_resCategorieValidateur["Nom_Obligatoire"])
            .MaximumLength(35).WithMessage(_resCategorieValidateur["Nom_LongueurMax"]);

        RuleFor(i => i.Description).Cascade(CascadeMode.Stop)            
            .MaximumLength(50).WithMessage(_resCategorieValidateur["Description_LongueurMax"]);
    }

    public async Task<ValidationModel> ValiderAsync(CategorieModel modele)
    {
        ValidationResult validationResult = await base.ValidateAsync(modele);

        return validationResult.VersValidationModel();
    }

    /// <summary>
    /// Valider une chaine de caractère qui est obligatoire
    /// </summary>
    /// <param name="valeur">Chaine à valider</param>
    /// <returns>Vrai si valide, faux si non valide</returns>
    private bool ValiderStringObligatoire(string valeur)
    {
        return !string.IsNullOrWhiteSpace(valeur);
    }
}
