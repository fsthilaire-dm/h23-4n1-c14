﻿namespace SuperCarte.Core.Models;

/// <summary>
/// Classe qui contient l'information de l'utilisateur authentifié
/// </summary>
public class UtilisateurAuthentifieModel
{
    public UtilisateurAuthentifieModel()
    {
        DateConnexion = DateTime.Now;
    }

    public int UtilisateurId { get; init; }

    public string Prenom { get; init; } = null!;

    public string Nom { get; init; } = null!;

    public string NomUtilisateur { get; init; } = null!;

    public DateTime DateConnexion { get; private set; }
}