﻿namespace SuperCarte.Core.Models;

/// <summary>
/// Classe qui contient un couple Valeur et Texte pour les items génériques
/// </summary>
/// <typeparam name="TValeur"></typeparam>
public class ListeItem<TValeur>
{
    public TValeur Valeur { get; set; }

    public string Texte { get; set; }
}
