﻿using SuperCarte.Core.Models;
using SuperCarte.Core.Repositories;

namespace SuperCarte.Core.Services;

/// <summary>
/// Classe qui contient les services du modèle Role
/// </summary>
public class RoleService : IRoleService
{
    private readonly IRoleRepo _roleRepo;

    /// <summary>
    /// Constructeur
    /// </summary>
    /// <param name="roleRepo">Repository Role</param>
    public RoleService(IRoleRepo roleRepo)
    {
        _roleRepo = roleRepo;
    }

    public List<ListeItem<int>> ObtenirListeItem()
    {
        return _roleRepo.ObtenirListeItem();
    }
}
