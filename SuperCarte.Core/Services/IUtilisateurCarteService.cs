﻿using SuperCarte.Core.Models;

namespace SuperCarte.Core.Services;

/// <summary>
/// Interface qui contient les services du modèle UtilisateurCarte
/// </summary>
public interface IUtilisateurCarteService
{
    /// <summary>
    /// Obtenir la liste des cartes d'un utilisateur avec sa quantité en asynchrone.
    /// La liste est triée par le nom de la catégorie et ensuite par le nom de la carte.
    /// </summary>
    /// <param name="utilisateurId">La clé de l'utilisateur</param>
    /// <returns>Liste des cartes avec la quantité</returns>
    Task<List<QuantiteCarteDetailModel>> ObtenirCartesUtilisateurAsync(int utilisateurId);

    /// <summary>
    /// Obtenir un UtilisateurCarteModel spécifique en fonction de sa clé primaire en asynchrone.
    /// </summary>
    /// <param name="utilisateurId">Clé primaire de l'utilisateur</param>
    /// <param name="carteId">Clé primaire de la carte</param>
    /// <returns>L'enregistrement UtilisateurCarteModel ou null si non trouvé</returns>
    Task<UtilisateurCarteModel?> ObtenirAsync(int utilisateurId, int carteId);

    /// <summary>
    /// Obtenir un UtilisateurCarteModel spécifique en fonction de sa clé primaire.
    /// </summary>
    /// <param name="utilisateurId">Clé primaire de l'utilisateur</param>
    /// <param name="carteId">Clé primaire de la carte</param>
    /// <returns>L'enregistrement UtilisateurCarteModel ou null si non trouvé</returns>
    UtilisateurCarteModel? Obtenir(int utilisateurId, int carteId);

    /// <summary>
    /// Obtenir la liste des cartes disponibles pour un utilisateur dans un objet ListeItem
    /// </summary>
    /// <returns>Liste des cartes disponible pour un utilisateur dans un ListeItem</returns>
    List<ListeItem<int>> ObtenirCartesDisponibles(int utilisateurId);

    /// <summary>
    /// Ajouter une carte à un utilisateur en asynchrone.
    /// </summary>
    /// <param name="utilisateurCarteModel">UtilisateurCarte à ajouter</param>     
    /// <returns>Vrai si ajoutée, faux si non ajoutée</returns>
    Task<bool> AjouterAsync(UtilisateurCarteModel utilisateurCarteModel);

    /// <summary>
    /// Modifier une carte à un utilisateur en asynchrone.
    /// </summary>
    /// <param name="utilisateurCarteModel">UtilisateurCarte à modifier</param>     
    /// <returns>Vrai si ajoutée, faux si non ajoutée</returns>
    Task<bool> ModifierAsync(UtilisateurCarteModel utilisateurCarteModel);

    /// <summary>
    /// Valider le modèle
    /// </summary>
    /// <param name="utilisateurCarteModel">UtilisateurCarteModel à valider</param>
    /// <param name="validerPourAjout">Valider pour un ajout d'un nouvel item</param>
    /// <returns>Résultat de validation</returns>
    Task<ValidationModel> ValiderAsync(UtilisateurCarteModel utilisateurCarteModel, bool validerPourAjout);

    /// <summary>
    /// Obtenir un UtilisateurCarteDetailModel spécifique en fonction de sa clé primaire.
    /// </summary>
    /// <param name="utilisateurId">Clé primaire de l'utilisateur</param>
    /// <param name="carteId">Clé primaire de la carte</param>
    /// <returns>L'objet UtilisateurCarteDetailModel ou null si non trouvé</returns>
    UtilisateurCarteDetailModel? ObtenirDetail(int utilisateurId, int carteId);
}