﻿using SuperCarte.Core.Extensions;
using SuperCarte.Core.Models;
using SuperCarte.Core.Repositories;
using SuperCarte.Core.Validateurs;
using SuperCarte.EF.Data;

namespace SuperCarte.Core.Services;

/// <summary>
/// Classe qui contient les services du modèle Categorie
/// </summary>
public class CategorieService : ICategorieService
{
    private readonly ICategorieRepo _categorieRepo;
    private readonly IValidateur<CategorieModel> _validateur;

    /// <summary>
    /// Constructeur
    /// </summary>
    /// <param name="categorieRepo">Repository Categorie</param>
    /// <param name="validateur">Validateur Categorie</param>
    public CategorieService(ICategorieRepo categorieRepo, IValidateur<CategorieModel> validateur)
    {
        _categorieRepo = categorieRepo;
        _validateur = validateur;
    }

    public async Task<List<CategorieModel>> ObtenirListeAsync()
    {
        return (await _categorieRepo.ObtenirListeAsync()).VersCategorieModel();
    }

    public CategorieDependance? ObtenirDependance(int categorieId)
    {
        return _categorieRepo.ObtenirDependance(categorieId);
    }    

    public async Task SupprimerAsync(int categorieId)
    {
        CategorieDependance? categorieDependance = await _categorieRepo.ObtenirDependanceAsync(categorieId);

        if(categorieDependance != null)
        {
            if(categorieDependance.NbCartes == 0)
            {
                await _categorieRepo.SupprimerParCleAsync(categorieId, true);
            }
            else
            {
                throw new Exception("La catégorie a des dépendances. Impossible à supprimer.");
            }
        }
        else
        {
            throw new Exception("La catégorie n'existe pas dans la base de données.");
        }
    }

    public async Task<bool> AjouterAsync(CategorieModel categorieModel)
    {
        if ((await _validateur.ValiderAsync(categorieModel)).EstValide == true)
        {
            //Transformation de l'objet du modèle du domaine en objet du modèle de données
            Categorie categorie = categorieModel.VersCategorie();

            //Ajout dans repository avec enregistrement immédiat
            await _categorieRepo.AjouterAsync(categorie, true);

            //Assigne les valeurs de la base de données dans l'objet du modèle
            categorieModel.Copie(categorie, true);

            return true;
        }
        else
        {
            return false;
        }
    }

    public async Task<CategorieModel?> ObtenirAsync(int categorieId)
    {
        Categorie? categorie = await _categorieRepo.ObtenirParCleAsync(categorieId);
      
        //Le ?. est important, car si la categorie n'est pas trouvée, l'objet sera null
        return categorie?.VersCategorieModel();
    }

    public CategorieModel? Obtenir(int categorieId)
    {
        Categorie? categorie = _categorieRepo.ObtenirParCle(categorieId);

        //Le ?. est important, car si la categorie n'est pas trouvée, l'objet sera null
        return categorie?.VersCategorieModel();
    }

    public async Task<bool> ModifierAsync(CategorieModel categorieModel)
    {
        if ((await _validateur.ValiderAsync(categorieModel)).EstValide == true)
        {
            Categorie? categorie = await _categorieRepo.ObtenirParCleAsync(categorieModel.CategorieId);

            if (categorie != null)
            {
                //Assigner les valeurs dans la catégorie
                categorie.Copie(categorieModel);

                await _categorieRepo.EnregistrerAsync();

                //Assigne les valeurs de la base de données dans l'objet du modèle
                categorieModel.Copie(categorie, false);
            }
            else
            {
                throw new Exception("Impossible de modifier la catégorie. Aucune catégorie trouvée avec la clé primaire.");
            }

            return true;
        }
        else
        {
            return false;
        }
    }

    public async Task<ValidationModel> ValiderAsync(CategorieModel categorieModel)
    {
        return await _validateur.ValiderAsync(categorieModel);
    }
}
