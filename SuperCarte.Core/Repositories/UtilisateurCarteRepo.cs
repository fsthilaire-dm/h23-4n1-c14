﻿using Microsoft.EntityFrameworkCore;
using SuperCarte.Core.Models;
using SuperCarte.Core.Repositories.Bases;
using SuperCarte.EF.Data;
using SuperCarte.EF.Data.Context;

namespace SuperCarte.Core.Repositories;

/// <summary>
/// Classe qui contient les méthodes de communication avec la base de données pour la table UtilisateurCarte
/// </summary>
public class UtilisateurCarteRepo : BaseRepo<UtilisateurCarte>, IUtilisateurCarteRepo
{
    /// <summary>
    /// Constructeur
    /// </summary>
    /// <param name="bd">Contexte de la base de données</param>
    public UtilisateurCarteRepo(SuperCarteContext bd) : base(bd)
    {
        //Vide, il sert uniquement a recevoir le contexte et à l'envoyer à la classe parent.
    }

    public async Task<List<QuantiteCarteDetailModel>> ObtenirCartesUtilisateurAsync(int utilisateurId)
    {
        return await (from lqUtilisateurCarte in _bd.UtilisateurCarteTb
                      where
                          lqUtilisateurCarte.UtilisateurId == utilisateurId
                      orderby
                          lqUtilisateurCarte.Carte.Categorie.Nom,                          
                          lqUtilisateurCarte.Carte.Nom
                      select
                          new QuantiteCarteDetailModel()
                          {
                              CarteId = lqUtilisateurCarte.Carte.CarteId,
                              Nom = lqUtilisateurCarte.Carte.Nom,
                              Vie = lqUtilisateurCarte.Carte.Vie,
                              Armure = lqUtilisateurCarte.Carte.Armure,
                              Attaque = lqUtilisateurCarte.Carte.Attaque,
                              EstRare = lqUtilisateurCarte.Carte.EstRare,
                              PrixRevente = lqUtilisateurCarte.Carte.PrixRevente,
                              CategorieId = lqUtilisateurCarte.Carte.CategorieId,
                              CategorieNom = lqUtilisateurCarte.Carte.Categorie.Nom,
                              EnsembleId = lqUtilisateurCarte.Carte.EnsembleId,
                              EnsembleNom = lqUtilisateurCarte.Carte.Ensemble.Nom,
                              Quantite = lqUtilisateurCarte.Quantite
                          }).ToListAsync();
    }

    public async Task<UtilisateurCarte?> ObtenirParCleAsync(int utilisateurId, int carteId)
    {
        return await (from lqUtilisateurCarte in _bd.UtilisateurCarteTb
                      where
                           lqUtilisateurCarte.UtilisateurId == utilisateurId &&
                           lqUtilisateurCarte.CarteId == carteId
                      select
                        lqUtilisateurCarte).FirstOrDefaultAsync();
    }

    public UtilisateurCarte? ObtenirParCle(int utilisateurId, int carteId)
    {
        return (from lqUtilisateurCarte in _bd.UtilisateurCarteTb
                where
                     lqUtilisateurCarte.UtilisateurId == utilisateurId &&
                     lqUtilisateurCarte.CarteId == carteId
                select
                  lqUtilisateurCarte).FirstOrDefault();
    }

    public UtilisateurCarteDetailModel? ObtenirDetailParCle(int utilisateurId, int carteId)
    {
        return (from lqUtilisateurCarte in _bd.UtilisateurCarteTb
                where
                     lqUtilisateurCarte.UtilisateurId == utilisateurId &&
                     lqUtilisateurCarte.CarteId == carteId
                select
                  new UtilisateurCarteDetailModel()
                  {
                      UtilisateurId = lqUtilisateurCarte.UtilisateurId,
                      CarteId = lqUtilisateurCarte.CarteId,
                      Quantite = lqUtilisateurCarte.Quantite,
                      UtilisateurNom = lqUtilisateurCarte.Utilisateur.Nom,
                      UtilisateurPrenom = lqUtilisateurCarte.Utilisateur.Nom,
                      CarteNom = lqUtilisateurCarte.Carte.Nom,
                      EnsembleNom = lqUtilisateurCarte.Carte.Ensemble.Nom,
                      CategorieNom = lqUtilisateurCarte.Carte.Categorie.Nom
                  }).FirstOrDefault();
    }

    public List<ListeItem<int>> ObtenirCartesDisponibles(int utilisateurId)
    {
        //Requête pour avoir la liste complète des cartes
        var reqListeCartes = from lqCarte in _bd.CarteTb
                             select lqCarte;

        //Requête pour avoir la liste des cartes de l'utilisateur
        var reqListeCartesUtilisateur =
            from lqUtilisateurCarte in _bd.UtilisateurCarteTb
            where
                lqUtilisateurCarte.UtilisateurId == utilisateurId
            select
                lqUtilisateurCarte.Carte;

        //Différence entre les 2 listes
        return (from lqCarte in reqListeCartes.Except(reqListeCartesUtilisateur)
                select
                    new ListeItem<int>()
                    {
                        Valeur = lqCarte.CarteId,
                        Texte = $"{lqCarte.Nom} ({lqCarte.Categorie.Nom}) ({lqCarte.Ensemble.Nom})"
                    }).ToList();
    }
}
