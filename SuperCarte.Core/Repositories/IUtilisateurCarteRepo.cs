﻿using SuperCarte.Core.Models;
using SuperCarte.Core.Repositories.Bases;
using SuperCarte.EF.Data;

namespace SuperCarte.Core.Repositories;

/// <summary>
/// Interface qui contient les méthodes de communication avec la base de données pour la table Role
/// </summary>
public interface IUtilisateurCarteRepo : IBaseRepo<UtilisateurCarte>
{
    /// <summary>
    /// Obtenir la liste des cartes d'un utilisateur avec sa quantité en asynchrone.
    /// La liste est triée par le nom de la catégorie et ensuite par le nom de la carte.
    /// </summary>
    /// <param name="utilisateurId">La clé de l'utilisateur</param>
    /// <returns>Liste des cartes avec la quantité</returns>
    Task<List<QuantiteCarteDetailModel>> ObtenirCartesUtilisateurAsync(int utilisateurId);

    /// <summary>
    /// Obtenir un UtilisateurCarte spécifique en fonction de sa clé primaire en asynchrone.
    /// </summary>
    /// <param name="utilisateurId">Clé primaire de l'utilisateur</param>
    /// <param name="carteId">Clé primaire de la carte</param>
    /// <returns>L'enregistrement UtilisateurCarte ou null si non trouvé</returns>
    Task<UtilisateurCarte?> ObtenirParCleAsync(int utilisateurId, int carteId);

    /// <summary>
    /// Obtenir un UtilisateurCarte spécifique en fonction de sa clé primaire.
    /// </summary>
    /// <param name="utilisateurId">Clé primaire de l'utilisateur</param>
    /// <param name="carteId">Clé primaire de la carte</param>
    /// <returns>L'enregistrement UtilisateurCarte ou null si non trouvé</returns>
    UtilisateurCarte? ObtenirParCle(int utilisateurId, int carteId);       

    /// <summary>
    /// Obtenir un UtilisateurCarteDetailModel spécifique en fonction de sa clé primaire.
    /// </summary>
    /// <param name="utilisateurId">Clé primaire de l'utilisateur</param>
    /// <param name="carteId">Clé primaire de la carte</param>
    /// <returns>L'objet UtilisateurCarteDetailModel ou null si non trouvé</returns>
    UtilisateurCarteDetailModel? ObtenirDetailParCle(int utilisateurId, int carteId);


    /// <summary>
    /// Obtenir la liste des cartes disponibles pour un utilisateur dans un objet ListeItem
    /// </summary>
    /// <returns>Liste des cartes disponible pour un utilisateur dans un ListeItem</returns>
    List<ListeItem<int>> ObtenirCartesDisponibles(int utilisateurId);
}
