﻿using SuperCarte.Core.Models;
using SuperCarte.Core.Repositories.Bases;
using SuperCarte.EF.Data;

namespace SuperCarte.Core.Repositories;

/// <summary>
/// Interface qui contient les méthodes de communication avec la base de données pour la table Role
/// </summary>
public interface IRoleRepo : IBasePKUniqueRepo<Role,int>
{
    /// <summary>
    /// Obtenir la liste des rôles dans un objet listeItem
    /// </summary>
    /// <returns>Liste de rôles dans un ListItem</returns>
    List<ListeItem<int>> ObtenirListeItem();
}
