﻿using SuperCarte.Core.Models;
using SuperCarte.Core.Repositories.Bases;
using SuperCarte.EF.Data;

namespace SuperCarte.Core.Repositories;

/// <summary>
/// Interface qui contient les méthodes de communication avec la base de données pour la table Carte
/// </summary>
public interface ICarteRepo : IBasePKUniqueRepo<Carte, int>
{
    /// <summary>
    /// Obtenir la liste des cartes avec le modèle CarteDetailModel en asynchrone.
    /// </summary>
    /// <returns>Liste des cartes</returns>
    Task<List<CarteDetailModel>> ObtenirListeCarteDetailAsync();

}
