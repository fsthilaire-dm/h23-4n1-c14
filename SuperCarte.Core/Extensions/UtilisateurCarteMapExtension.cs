﻿using SuperCarte.Core.Models;
using SuperCarte.EF.Data;

namespace SuperCarte.Core.Extensions;

/// <summary>
/// Classe statique qui regroupe les méthodes d'extension pour la conversion (mapping) du modèle UtilisateurCarte
/// </summary>
public static class UtilisateurCarteMapExtension
{
    /// <summary>
    /// Convertir un objet UtilisateurCarte vers un objet UtilisateurCarteModel
    /// </summary>
    /// <param name="item">Objet à convertir</param>
    /// <returns>Objet converti</returns>
    public static UtilisateurCarteModel VersUtilisateurCarteModel(this UtilisateurCarte item)
    {
        return new UtilisateurCarteModel()
        {
            UtilisateurId = item.UtilisateurId,
            CarteId = item.CarteId,
            Quantite = item.Quantite
        };
    }

    /// <summary>
    /// Convertir un objet UtilisateurCarteModel vers un objet UtilisateurCarte
    /// </summary>
    /// <param name="item">Objet à convertir</param>
    /// <returns>Objet converti</returns>
    public static UtilisateurCarte VersUtilisateurCarte(this UtilisateurCarteModel item)
    {
        return new UtilisateurCarte()
        {
            UtilisateurId = item.UtilisateurId,
            CarteId = item.CarteId,
            Quantite = item.Quantite
        };
    }

    /// <summary>
    /// Méthode qui copie les valeurs des propriétés de l'objet de donnée UtilisateurCarte dans l'objet du modèle UtilisateurCarteModel
    /// </summary>
    /// <param name="itemDestination">UtilisateurCarteModel à recevoir la copie (destination)</param>
    /// <param name="utilisateurCarteSource">L'objet UtilisateurCarte de référence pour la copie (source)</param>
    /// <param name="copierClePrimaire">Copier de la clé primaire</param>
    public static void Copie(this UtilisateurCarteModel itemDestination, UtilisateurCarte utilisateurCarteSource, bool copierClePrimaire)
    {
        if (copierClePrimaire == true)
        {
            itemDestination.UtilisateurId = utilisateurCarteSource.UtilisateurId;
            itemDestination.CarteId = utilisateurCarteSource.CarteId;
        }

        itemDestination.Quantite = utilisateurCarteSource.Quantite;
    }

    /// <summary>
    /// Méthode qui copie les valeurs des propriétés du UtilisateurCarteModel dans l'objet de donnée UtilisateurCarte
    /// </summary>
    /// <param name="itemDestination">UtilisateurCarte à recevoir la copie (destination)</param>
    /// <param name="utilisateurCarteModelSource">L'objet UtilisateurCarteModel de référence pour la copie (source)</param>
    /// <param name="ignoreClePrimaire">Ignore la copie de la clé primaire</param>
    public static void Copie(this UtilisateurCarte itemDestination, UtilisateurCarteModel utilisateurCarteModelSource, bool ignoreClePrimaire = true)
    {
        if (ignoreClePrimaire == true)
        {
            itemDestination.UtilisateurId = utilisateurCarteModelSource.UtilisateurId;
            itemDestination.CarteId = utilisateurCarteModelSource.CarteId;
        }

        itemDestination.Quantite = utilisateurCarteModelSource.Quantite;
    }
}